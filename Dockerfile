FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /secondproject
WORKDIR /secondproject
RUN pip install pip -U
RUN pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
ADD requirements.txt /secondproject
RUN pip install -r requirements.txt
ADD ./secondproject /secondproject