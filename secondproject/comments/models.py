from django.db import models

# Create your models here.
class Comment(models.Model):
    name = models.CharField(max_length=50)
    time = models.DateTimeField(auto_now=True)
    text = models.TextField(max_length=1000)
    img = models.ImageField(upload_to='media/')